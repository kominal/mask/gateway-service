export const STACK_NAME = process.env.STACK_NAME;
export const LISTEN_ADDRESS = process.env.LISTEN_ADDRESS || '0.0.0.0';
export const LISTEN_PORT_4 = Number(process.env.LISTEN_PORT_4 || '3000');
export const LISTEN_PORT_6 = Number(process.env.LISTEN_PORT_6 || '3001');
